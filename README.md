<div align="center">
  <h1>@dano/prettier-task</h1>
  <p>Prettier 설치부터 설정까지 한 번에 🚀</p>
  <br/>
</div>

## 사용 방법

```bash
$ npx -p @dano/prettier-task -p mrm -c 'mrm @dano/prettier-task'
```

## 설치되는 패키지 목록

- `prettier`
- `@dano/prettier-config`
- `prettier-plugin-package`
- `prettier-plugin-optimize-imports` (타입스크립트 사용시)
- `lint-staged`
- `husky`

## 적용되는 내용 (package.json)

- `prettier` 항목을 `@dano/prettier-config`으로 설정
- `husky`의 `pre-commit`을 `lint-staged`로 설정
- `lint-staged`에 `*.{js,jsx}` 패턴 파일에 `prettier --write` 실행
