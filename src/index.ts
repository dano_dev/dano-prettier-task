import { packageJson, install } from 'mrm-core';

function getLintStagedRules(useTypescript: boolean) {
  const extensions = [
    'js',
    'jsx',
    ...(useTypescript ? ['ts', 'tsx'] : []),
  ].filter(Boolean);
  const extensionsGlob = `*.{${extensions.join(',')}}`;

  return {
    [extensionsGlob]: ['prettier --write'],
  };
}

function task() {
  const pkg = packageJson();

  const useTypescript =
    pkg.get('devDependencies.typescript') || pkg.get('dependencies.typescript');

  pkg
    .unset('scripts.precomit')
    .merge({
      husky: {
        hooks: {
          'pre-commit': 'lint-staged',
        },
      },
      'lint-staged': getLintStagedRules(useTypescript),
      prettier: '@dano/prettier-config',
    })
    .save();

  const packages = [
    'prettier',
    '@dano/prettier-config',
    'prettier-plugin-package',
    useTypescript && 'prettier-plugin-organize-imports',
    'lint-staged',
    'husky',
  ].filter(Boolean);

  install(packages);
}

task.description = '';

export = task;
